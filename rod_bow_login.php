<?php
//ユーザー名とパスワードの対応
$users = array(
	"username" => "password"
);

//セッションスタート
session_start();

//データベースへの接続
$pdo = new PDO(""); 

//入力チェック
if(isset($_POST["username"])){
	//値の取得
	$username = $_POST["username"];
	$password = $_POST["password"];
	$autologin = $_POST["autologin"];
	
	//ユーザー名、パスワードがあっているかを確認
	if(isset($users[$username]) && $users[$username] == $password){
		$_SESSION["login"] = $username;
		header("location: rod_bow_menu.php");
		
		//自動ログインチェックがある場合
		if(isset($autologin)){
			//すでに存在するトークンを削除
			$query1 = " DELETE FROM auto_login WHERE username = '{$username}'";
			$pdo -> query($query1);
			
			//トークンを生成
			$token = $username.time();
			
			//トークンをデータベースへ保存
			$query2 = " INSERT INTO auto_login VALUES('{$username}','{$token}')";
			$stmt2 = $pdo -> query($query2);
			
			if($stmt2 != false){
				//トークンをクッキーに一週間保存
				setcookie("token","$token",time() + 7*60*60*24);
			}
		}
		
	}else{
		$error = "ユーザー名かパスワードが正しくありません";
	}
}

//クッキーにトークンがある場合
if(isset($_COOKIE["token"])){
	//トークンに対応するログイン情報を検索
	$query = " SELECT * FROM auto_login WHERE token = '{$_COOKIE["token"]}'";
	$stmt = $pdo -> query($query);
	if($stmt != false){
		$row = $stmt -> fetch(PDO::FETCH_ASSOC);
		$_SESSION["login"] = $row["username"];
		
		//トークンを作り直す
		$query2 = " DELETE FROM auto_login WHERE token = '{$_COOKIE["token"]}'";
		$pdo -> query($query2);
		$new_token = $_SESSION["token"].time();
		$query3 = " INSERT INTO auto_login VALUES ('{$_SESSION["login"]}','{$new_token}')";
		$stmt3 = $pdo -> query($query3);
		if($stmt3 != false){
			setcookie("token","$new_token",time() + 7*60*60*24);
		}
		
		header("Location: rod_bow_manu.php");
		
	}
}

?>

<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>ロッド棒管理アプリ　ログインページ</title>
<link href="jquery-mobile/jquery.mobile-1.3.0.min.css" rel="stylesheet" type="text/css">
<script src="jquery-mobile/jquery-1.11.1.min.js"></script>
<script src="jquery-mobile/jquery.mobile-1.3.0.min.js"></script>
</head>

<body>
<div data-role="page" id="page">
  <div data-role="header">
    <h1>ログインページ</h1>
  </div>
  <div data-role="content">
	<form id="form1" name="form1" method="post" action="">
		
      <div data-role="fieldcontain">
        <label for="textinput">  ユーザー名 :　</label>
        <input type="text" name="username" id="username" />
      </div><br />
		
      <div data-role="fieldcontain">
        <label for="passwordinput">パスワード入力 : </label>
        <input type="password" name="password" id="password" />
      </div><br />
      <div data-role="fieldcontain">
        <fieldset data-role="controlgroup">
          <legend>次回から自動ログイン</legend>
          <input type="checkbox" name="autologin" id="autologin" class="custom" value="autologin" />
          <label for="autologin">autologin</label>
        </fieldset>
      </div>
<input type="submit" name="submit" id="submit" value="ログイン" />
	</form>
	</div>
  <div data-role="footer">
    <h4>(c)emuemu-project</h4>
  </div>
</div>
	
	<?php echo $error; ?>	
</body>
</html>