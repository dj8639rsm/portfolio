<?php
//セッションスタート
session_start();

//ログインしているかの確認
if(!isset($_SESSION["login"])){
	//ログイン画面に転送
	header("location: rod_bow_login.php");
}

?>

<!doctype html>
<html>
<head>
	<meta charset="UTF-8">
	<title> ロッド棒管理アプリ　入力画面</title>
	<link href="jquery-mobile/jquery.mobile-1.3.0.min.css" rel="stylesheet" type="text/css">
	<script src="jquery-mobile/jquery-1.11.1.min.js"></script>
	<script src="jquery-mobile/jquery.mobile-1.3.0.min.js"></script>
</head>

<body>
	<p align="right"><a href="rod_bow_menu.php">メニュー画面へ</a></p><br />
	<p align="right"><a href="rod_bow_logout.php">ログアウト</a></p><br />
	<form id="form1" name="form1" method="post" action="">
		客先名：<input type="text" name="makername" id="makername"/><br/> 
		製品名称：<input type="text" name="serialnumber" id="serialnumber"/><br/> 
		工程番号：<input type="number" name="processnumber" id="processnumber"/><br/>
		<hr> 
		ロッド棒は全長を入力してください（穴から計測した数値のプラス７ミリ）<br/> 
		ロッド棒の材質：
		<select name="rod_material" id="rod_material">
			<option value="鉄">鉄</option>
			<option value="ステンレス">ステンレス</option>
			<option value="その他">その他</option>
		</select><br/> 
		ロッド棒の太さ：
		<select name="rod_thick" id="rod_thick">
			<option value="１０パイ">１０パイ</option>
			<option value="８パイ">８パイ</option>
			<option value="６パイ">６パイ</option>
			<option value="その他">その他</option>
		</select><br/> 
		ロッド棒全長（上）：<input type="number" name="uprod" id="uprod"/><br/> 
		ロッド棒曲げ値（上）：<input type="number" name="benduprod" id="benduprod"/><br/> 
		ロッド棒三次元曲げ（上）：
		<select name="uprod3d" id="uprod3d">
			<option value="あり">なし</option>
			<option value="なし">あり</option>		
		</select><br />
		ロッド棒全長（下）：<input type="number" name="downrod" id="downrod"/><br/> 
		ロッド棒曲げ値（下）：<input type="number" name="benddownrod" id="benddownrod"/><br/>
		ロッド棒三次元曲げ（下）：
		<select name="downrod3d" id="downrod3d">
			<option value="あり">なし</option>
			<option value="なし">あり</option>		
		</select><br />
		<br/> 
		ベロの材質：
		<select name="bero_material" id="bero_material">
			<option value="鉄">鉄</option>
			<option value="ステンレス">ステンレス</option>
			<option value="その他">その他</option>
		</select><br/>
		ベロの種類：
		<select name="bero_type" id="bero_type">
			<option value="８角ベロ">８角ベロ</option>
			<option value="８角長ベロ">８角長ベロ</option>
			<option value="１０角ベロ">１０角ベロ</option>
			<option value="１０角長ベロ">１０角長ベロ</option>
			<option value="その他">その他</option>
		</select><br />
		ベロの曲げ値：<input type="number" name="bendbero" id="bendbero"><br />
		ベロの曲げ方向
		<select name="berodir" id="berodir">
			<option value="右扉・奥向き">右扉・奥向き</option>
			<option value="左扉・奥向き">左扉・奥向き</option>
			<option value="右扉・手前向き">右扉・手前向き</option>
			<option value="左扉・手前向き">左扉・手前向き</option>
		</select><br />
		ベロのカット量：<input type="number" name="berocut" id="berocut"><br />
		<br />
		<input type="submit" name="submit" id="submit" value="データを保存">
	</form>
	
	<?php
	//入力チェック
	if(!isset($_POST["submit"])){
		exit();
	}
		
	//値の取得
	$makername = htmlspecialchars($_POST["makername"],ENT_QUOTES);
	$serialnumber = htmlspecialchars($_POST["serialnumber"],ENT_QUOTES);
	$processnumber = htmlspecialchars($_POST["processnumber"],ENT_QUOTES);
	$rod_material = htmlspecialchars($_POST["rod_material"],ENT_QUOTES);
	$rod_thick = htmlspecialchars($_POST["rod_thick"],ENT_QUOTES);
	$uprod = htmlspecialchars($_POST["uprod"],ENT_QUOTES);
	$benduprod = htmlspecialchars($_POST["benduprod"],ENT_QUOTES);
	$uprod3d = htmlspecialchars($_POST["uprod3d"],ENT_QUOTES);
	$downrod = htmlspecialchars($_POST["downrod"],ENT_QUOTES);
	$benddownrod = htmlspecialchars($_POST["benddownrod"],ENT_QUOTES);
	$downrod3d = htmlspecialchars($_POST["downrod3d"],ENT_QUOTES);
	$bero_material = htmlspecialchars($_POST["bero_material"],ENT_QUOTES);
	$bero_type = htmlspecialchars($_POST["bero_type"],ENT_QUOTES);
	$bendbero = htmlspecialchars($_POST["bendbero"],ENT_QUOTES);
	$berodir = htmlspecialchars($_POST["berodir"],ENT_QUOTES);
	$berocut = htmlspecialchars($_POST["berocut"],ENT_QUOTES);
	$date = date("Y/m/d");
	
	//データベースへの接続
	$pdo = new PDO(""); 
	
	//文字コードの指定
	$pdo -> query("set character set utf8");
	
	//SQL文の編集
	$query = " INSERT INTO rod_bow_db VALUES(
	null, 
	'{$makername}',
	'{$serialnumber}',
	'{$processnumber}',
	'{$rod_material}',
	'{$rod_thick}',
	'{$uprod}',
	'{$benduprod}',
	'{$uprod3d}',
	'{$downrod}',
	'{$benddownrod}',
	'{$downrod3d}',
	'{$bero_material}',
	'{$bero_type}',
	'{$bendbero}',
	'{$berodir}',
	'{$berocut}',
	'{$date}'
	)";
	
	//SQL文の実行
	$stmt = $pdo -> query($query);
	
	//入力判定
	if($stmt == false){
		$error = $pdo -> errorInfo();
		echo $error[2] . "<br />\n";
		echo "データを保存できませんでした";
	}else{
		echo "データを保存しました";
	}	
	
	?>
</body>
</html>