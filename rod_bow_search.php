<?php
//セッションスタート
session_start();

//ログインしているかの確認
if(!isset($_SESSION["login"])){
	//ログイン画面に転送
	header("location: rod_bow_login.php");
}

?>

<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>rロッド棒管理アプリ　検索画面</title>
<link href="jquery-mobile/jquery.mobile-1.3.0.min.css" rel="stylesheet" type="text/css">
<script src="jquery-mobile/jquery-1.11.1.min.js"></script>
<script src="jquery-mobile/jquery.mobile-1.3.0.min.js"></script>
</head>

<body>
	<p align="right"><a href="rod_bow_menu.php">メニュー画面へ</a></p><br />
	<p align="right"><a href="rod_bow_logout.php">ログアウト</a></p><br />
	
	<form id="form" name="form" method="post" action="">
	製品名称:
	<input type="text" name="serialnumber" id="serialnumber" />
	<input type="submit" name="submit" id="submit" value="検索"/>
	</form>
	
	<?php
	//入力チェック
	if(!isset($_POST["submit"])){
		exit();
	}
	
	//値の取得
	$serialnumber = htmlspecialchars($_POST["serialnumber"],ENT_QUOTES);
	
	//データベースへの接続
	$pdo = new PDO("");
	
	//文字コードの設定
	$pdo -> query("set character set utf8");
	
	//SQL文を編集・科目の編集
	$query = " SELECT * FROM rod_bow_db WHERE serialnumber like '%{$serialnumber}%' ";
	
	//SQL文の実行
	$stmt = $pdo -> query($query);
	
	if($stmt == false){
		$error = $pdo -> errorInfo();
		echo $error[2] . "<br />\n";
		exit("データが取得できません<br />");
	}
	
	//データベース（変数）からの出力
while($row = $stmt -> fetch(PDO::FETCH_ASSOC)){

echo <<< END
<table border = "1">
<tr><td>ID</td><td>{$row["ID"]}</td></tr>
<tr><td>客先名</td><td>{$row["makername"]}</td></tr>
<tr><td>製品名称</td><td>{$row["serialnumber"]}</td></tr>
<tr><td>工程番号</td><td>{$row["processnumber"]}</td></tr>
<tr><td>ロッド棒の材質</td><td>{$row["rod_material"]}</td></tr>
<tr><td>ロッド棒の太さ</td><td>{$row["rod_thick"]}</td></tr>
<tr><td>ロッド棒の長さ（上）</td><td>{$row["uprod"]}</td></tr>
<tr><td>ロッド棒曲げ値（上）</td><td>{$row["benduprod"]}</td></tr>
<tr><td>三次元曲げ（上）</td><td>{$row["uprod3d"]}</td></tr>
<tr><td>ロッド棒の長さ（下）</td><td>{$row["downrod"]}</td></tr>
<tr><td>ロッド棒曲げ値（下）</td><td>{$row["benddownrod"]}</td></tr>
<tr><td>三次元曲げ（下）</td><td>{$row["downrod3d"]}</td></tr>
<tr><td>ベロの材質</td><td>{$row["bero_material"]}</td></tr>
<tr><td>ベロの種類</td><td>{$row["bero_type"]}</td></tr>
<tr><td>ベロの曲げ値</td><td>{$row["bendbero"]}</td></tr>
<tr><td>ベロの曲げ方向</td><td>{$row["berodir"]}</td></tr>
<tr><td>ベロのカット値</td><td>{$row["berocut"]}</td></tr>
<tr><td>日付</td><td>{$row["date"]}</td></tr>
END;
}
echo "</table>\n"	
	
	?>
	
</body>
</html>