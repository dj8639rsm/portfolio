<?php
//セッションを開始
session_start();

//ログイン情報の消去
session_destroy();

//トークンがある場合、データベースおよびクッキーから削除しておく
if(isset($_COOKIE["token"])){
	//データベースへの接続
	$pdo = new PDO(""); 
	
	//データベースのトークンを削除
	$query = "DELETE FROM auto_login WHERE token = '{$_COOKIE["token"]}'";
	$pdo -> query($query);
	
	//クッキーのトークンを削除
	setcookie("token","");
}

//ログイン画面にリダレクト
header("Location: rod_bow_login.php");

?>