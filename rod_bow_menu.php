<?php
//セッションスタート
session_start();

//ログインしているかの確認
if(!isset($_SESSION["login"])){
	//ログイン画面に転送
	header("location: rod_bow_login.php");
}

?>

<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>ロッド棒管理アプリ　メニュー画面</title>
<link href="jquery-mobile/jquery.mobile-1.3.0.min.css" rel="stylesheet" type="text/css">
<script src="jquery-mobile/jquery-1.11.1.min.js"></script>
<script src="jquery-mobile/jquery.mobile-1.3.0.min.js"></script>
</head>

<body>
<div data-role="page" id="page">
  	<div data-role="content">
		<h2>ロッド棒管理アプリ トップページ </h2>
	  	<p align="right"><a href="rod_bow_logout.php">ログアウト</a></p>
	</div>
 	<div> 
		<ul data-role="listview" data-split-icon="arrow-r">
    		<li><a href="rod_bow_search.php" data-transition="flip">データ検索</a></li>
    		<li><a href="rod_bow_input.php" data-transition="flip">データ入力</a></li>
  		</ul>
	</div>
  <div data-role="footer">
    <h4>(c)emuemu-project</h4>
  </div>
</div>
</body>
</html>